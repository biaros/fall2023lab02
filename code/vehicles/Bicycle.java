// Bianca Rossetti 2233420
package vehicles;
public class Bicycle 
{
    private String manufacturer;
    private int numGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numGears, double maxSpeed)
    {
        this.manufacturer = manufacturer;
        this.numGears = numGears;
        this.maxSpeed = maxSpeed;
    }
    public String getMaufacturer()
    {
        return(this.manufacturer);
    }

    public int getNumGears()
    {
        return(this.numGears);
    }

    public double getMaxSpeed()
    {
        return(this.maxSpeed);
    }

    public String toString()
    {
        String s = "Manufacturer: " +  this.manufacturer + ", Number of Gears: " + this.numGears + ", MaxSpeed: " + this.maxSpeed;
        return(s);
    }
}