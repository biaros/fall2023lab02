// Bianca Rossetti - 2233420
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args)
    {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Specialized", 5, 30);
        bicycles[1] = new Bicycle("Garneau", 7, 50);
        bicycles[2] = new Bicycle("Superspeed", 6, 40);
        bicycles[3] = new Bicycle("RetroBikes", 6, 45);
        for (Bicycle b : bicycles)
        {
            System.out.println(b);
        }
    }
}
